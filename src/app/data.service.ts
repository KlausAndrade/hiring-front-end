import { Injectable } from '@angular/core'; 
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }
  getUser(username) {
    return this.http.get('https://api.github.com/search/users?q='+username)
  }
  
  getUsers() {
    return this.http.get('https://api.github.com/search/users?q=0')
  }
}
